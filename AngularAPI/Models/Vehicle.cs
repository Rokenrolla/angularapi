﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularAPI.Models
{
    public class Vehicle
    {
        public int id { get; set; }
        public string FuelType { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }
        public string ConstructionYear { get; set; }
        public string Driver { get; set; }
        public string Kilometers { get; set; }
    }
}
