import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { VehicleService } from '../vehicle.service';
import { Vehicle } from '../vehicle';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {
  dataSaved = false;
  vehicleForm: any;
  allVehicles: Observable<Vehicle[]>;
  vehicleIdUpdate = null;
  massage = null;

  constructor(private formbulider: FormBuilder, private vehicleService:VehicleService) { }

  ngOnInit() {
    this.vehicleForm = this.formbulider.group({
      FuelType: ['', [Validators.required]],
      Brand: ['', [Validators.required]],
      Type: ['', [Validators.required]],
      ConstructionYear: ['', [Validators.required]],
      Driver: ['', [Validators.required]],
      Kilometers: ['', [Validators.required]],
    });
    this.loadAllVehicles();
  }
  loadAllVehicles() {
    this.allVehicles = this.vehicleService.getAllVehicle();
  }
  onFormSubmit() {
    this.dataSaved = false;
    const vehicle = this.vehicleForm.value;
    this.CreateVehicle(vehicle);
    this.vehicleForm.reset();
  }
  loadVehicleToEdit(id: string) {
    this.vehicleService.getVehicleById(id).subscribe(vehicle=> {
      this.massage = null;
      this.dataSaved = false;
      this.vehicleIdUpdate = vehicle.id;
      this.vehicleForm.controls['FuelType'].setValue(vehicle.FuelType);
      this.vehicleForm.controls['Brand'].setValue(vehicle.Brand);
      this.vehicleForm.controls['Type'].setValue(vehicle.Type);
      this.vehicleForm.controls['ConstructionYear'].setValue(vehicle.ConstructionYear);
      this.vehicleForm.controls['Driver'].setValue(vehicle.Driver);
      this.vehicleForm.controls['Kilometers'].setValue(vehicle.Kilometers);
    });

  }
  CreateVehicle(vehicle: Vehicle) {
    if (this.vehicleIdUpdate == null) {
      this.vehicleService.createVehicle(vehicle).subscribe(
        () => {
          this.dataSaved = true;
          this.massage = 'Record saved Successfully';
          this.loadAllVehicles();
          this.vehicleIdUpdate = null;
          this.vehicleForm.reset();
        }
      );
    } else {
      vehicle.id = this.vehicleIdUpdate;
      this.vehicleService.updateVehicle(vehicle).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record Updated Successfully';
        this.loadAllVehicles();
        this.vehicleIdUpdate = null;
        this.vehicleForm.reset();
      });
    }
  }
  deleteVehicle(id: string) {
    if (confirm("Are you sure you want to delete this ?")) {
    this.vehicleService.deleteVehicleById(id).subscribe(() => {
      this.dataSaved = true;
      this.massage = 'Record Deleted Succefully';
      this.loadAllVehicles();
      this.vehicleIdUpdate = null;
      this.vehicleForm.reset();

    });
  }
}
  resetForm() {
    this.vehicleForm.reset();
    this.massage = null;
    this.dataSaved = false;
  }
}
