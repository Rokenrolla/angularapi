export class Vehicle {
  id:number;
  FuelType: string;
  Brand: string;
  Type: string;
  ConstructionYear: string;
  Driver: string;
  Kilometers: string;
}


