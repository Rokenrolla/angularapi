import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Vehicle } from './vehicle';


 @Injectable({
  providedIn: 'root'
})

export class VehicleService {
  url = 'http://localhost:57289/Api/Vehicles/';
  constructor(private http: HttpClient) { }
  getAllVehicle(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(this.url);
  }
  getVehicleById(vehicleId: string): Observable<Vehicle> {
    return this.http.get<Vehicle>(this.url + vehicleId);
  }
  createVehicle(vehicle: Vehicle): Observable<Vehicle> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };
    return this.http.post<Vehicle>(this.url, vehicle, httpOptions);
  }

  updateVehicle(vehicle: Vehicle): Observable<Vehicle> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };
    return this.http.put<Vehicle>(this.url+vehicle.id,vehicle, httpOptions);
  }
  deleteVehicleById(vehicleid: string): Observable<number> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };
    return this.http.delete<number>(this.url + +vehicleid,
 httpOptions);
  }
}
